import base64
import logging
import os
import pathlib
import sys

import gitlab
from gitlab import GitlabGetError
from gitlab.v4.objects import Project

GITLAB_SERVER = os.environ.get('GL_SERVER', 'https://gitlab.com')
GITLAB_TOKEN = os.environ.get('GL_TOKEN')
GROUP_ID = os.environ.get('GL_GROUP_ID', 6369256)


def manage_helm_projects(project: Project):
    logging.debug("enforcing default settings")
    project.public_jobs = False
    project.shared_runners_enabled = False
    project.wiki_enabled = True
    project.save()

    logging.debug("syncing files")
    parent_dir = "files/helm/"
    # remove
    for f in pathlib.Path(parent_dir).glob("*"):
        logging.debug(f"checking {f.name} file content")
        file_contents = f.read_text(encoding="utf-8")
        # get remote file contents
        try:
            remote_file = project.files.get(f.name, project.default_branch)
        except GitlabGetError as ge:
            # if file simply doesn't exist create a new one
            if ge.response_code == 404:
                remote_file = project.files.create({'file_path': f.name,
                                                    'branch': project.default_branch,
                                                    'content': file_contents,
                                                    'commit_message': f"(chore) updating {f.name} [ci-skip]"})
                continue
            raise ge
        if file_contents == base64.b64decode(remote_file.content).decode():
            logging.debug("file contents are identical, skipping it")
            continue
        logging.debug(f"file contents of {f.name} are different, syncing file contents up")
        remote_file.content = file_contents
        remote_file.save(project.default_branch, f"(chore) GitlabEnforcer: updating {f.name} [ci-skip]")
    return


if __name__ == "__main__":
    logging.basicConfig(level=os.environ.get('LOGLEVEL', 'DEBUG').upper())
    if not GITLAB_TOKEN:
        print("Please set the GL_TOKEN env variable.")
        sys.exit(1)

    gl = gitlab.Gitlab(GITLAB_SERVER, private_token=GITLAB_TOKEN)
    main_group = gl.groups.get(GROUP_ID)
    # get all projects
    for p in main_group.projects.list(include_subgroups=True, all=True):
        try:
            # skip archived projects
            if p.archived:
                logging.info(f"Project {p.name} is archived. Skipping")
                continue

            logging.info(f'running {p.name} project')
            # get full object
            # https://python-gitlab.readthedocs.io/en/stable/gl_objects/groups.html#examples
            project = gl.projects.get(p.id)

            # set default merging method to FF
            project.merge_method = "ff"

            # sync files
            # project.default_branch
            match project.namespace["name"]:
                case "helm":
                    manage_helm_projects(project)

            project.save()

            print("Project name: {p}".format(p=p.name))
        except Exception as e:
            print("Got exception: {e} \n ===================================== \n".format(e=e))
